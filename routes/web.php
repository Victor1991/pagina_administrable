<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Auth::routes();

Route::get('data_view_json', 'PageController@data_view_json')->name('data_view_json');
Route::get('testimonials/{id}', 'PageController@testimonials')->name('testimonials');
Route::post('register_appointment', 'Admin\QuotesController@register_appointment')->name('register_appointment');
Route::post('save_testimonial', 'PageController@save_testimonial')->name('save_testimonial');

Route::group([
     'prefix' => 'admin',
     'namespace' => 'Admin',
     'middleware' => 'auth'],
function(){
     Route::get('/', 'AdminController@index')->name('dashboard');
     Route::resource('user', 'UserController', ['as' => 'admin']);
     Route::resource('calendar', 'CalendarController', ['as' => 'admin']);
     Route::resource('quotes', 'QuotesController', ['as' => 'admin']);
     Route::resource('packages', 'PackagesController', ['as' => 'admin']);
     Route::resource('configuration', 'ConfigurationController', ['as' => 'admin']);
     Route::resource('testimonials', 'TestimonialsController', ['as' => 'admin']);
     Route::resource('gallery', 'GalleryController', ['as' => 'admin']);
     Route::resource('cleaning_need', 'CleaningNeedsController', ['as' => 'admin']);
     Route::resource('contact_images', 'ContactImagesController', ['as' => 'admin']);
     Route::resource('disable_day', 'DisableDayController', ['as' => 'admin']);
     Route::get('quotes/finish_quote/{id}', 'QuotesController@finish_quote')->name('admin.quotes.finish_quote');

});
