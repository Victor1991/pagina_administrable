<?php
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
          User::truncate();

          $admin = new User;
          $admin->name = 'Victor Hugo';
          $admin->last_name_paternal = 'Uribe';
          $admin->last_name_maternal = 'Hernandez';
          $admin->status = '1';
          $admin->email = 'vic.hug.urib@gmail.com';
          $admin->password = bcrypt('12345678');
          $admin->save();
     }
}
