<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinishQuotesUserEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $quotes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($quotes)
     {
         //
         $this->quotes = $quotes;

     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->subject('Finished service')->view('mails.finished_service');
    }
}
