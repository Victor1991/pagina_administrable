<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weekdays extends Model
{
     protected $fillable = [
        'name', 'name_en', 'package_id'
    ];
}
