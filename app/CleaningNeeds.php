<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CleaningNeeds extends Model
{
    //
    protected $fillable = [
         'name','icon','status','title','description','image'
   ];
}
