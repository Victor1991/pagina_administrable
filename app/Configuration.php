<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{

     protected $fillable = [
          'facebook','linkedin','instagram','twitter','youtube','snapchat','mail','address','phone'
    ];

}
