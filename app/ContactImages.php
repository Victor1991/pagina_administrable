<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactImages extends Model
{
     protected $fillable = [
          'photo', 'contact', 'status'
     ];
}
