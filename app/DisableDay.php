<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisableDay extends Model
{
    protected $fillable = [
        'day', 'status'
    ];
}
