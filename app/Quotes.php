<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
     //
     protected $fillable = [
          'package_id', 'name', 'visit', 'direction', 'information', 'phone', 'mail', 'finalized', 'price', 'cost', 'hours'
     ];


     public function testimonial()
     {
          return $this->hasOne(Testimonials::class, 'quote_id', 'id');
     }

     public function package()
     {
          return $this->hasOne(Packages::class, 'id', 'package_id');
     }
}
