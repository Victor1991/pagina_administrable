<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    //
    protected $fillable = [
       'quote_id', 'qualification', 'description', 'on_page'
   ];

   public function quote()
   {
        return $this->hasOne(Quotes::class,  'id','quote_id');
   }
}
