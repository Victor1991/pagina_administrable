<?php

namespace App\Http\Controllers\Admin;

use App\Packages;
use App\Tasks;
use App\Weekdays;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class PackagesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $packages = Packages::all();
          return view('admin.packages.index', compact('packages'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $packages = new Packages;
          $weekdays = Weekdays::all();
          return view('admin.packages.create', compact('packages', 'weekdays'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/'
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;

          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }

          $packages = Packages::create($data)->id;

          if (request('tasks') ) {
               foreach (request('tasks') as $key => $tasks) {
                    $task['name'] = $tasks;
                    $task['package_id'] = $packages;
                    Tasks::create($task);
               }
          }

          if (request('day')) {
               foreach (request('day') as $key => $day) {
                    $day_update = Weekdays::find($day);
                    $dta['package_id'] = $packages;
                    $day_update->update( $dta );
               }
          }


          return redirect()->route('admin.packages.index')->with('success', 'Paquete registrado');
     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Packages $packages)
     {
          return view('admin.packages.show', compact('packages'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $weekdays = Weekdays::all();
          $packages = Packages::find($id);
          return view('admin.packages.edit', compact('packages', 'weekdays'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
          $packages = Packages::find($id);

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/'
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;


          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }

          $packages->update( $data );


          if (request('tasks') ) {
               Tasks::whereNotIn('id', array_keys($request->input('tasks')))->where('package_id', $id)->delete();
               foreach (request('tasks') as $key => $tasks) {
                    $packages = Tasks::find($key);
                    $data_task['name'] = $tasks;
                    $packages->update( $data_task );
               }
          }

          if (request('n')) {
               foreach (request('n') as $key => $tasks) {
                    $data_task['name'] = $tasks;
                    $data_task['package_id'] = $id;
                    Tasks::create($data_task);
               }
          }

          if (request('day')) {
               foreach (request('day') as $key => $day) {
                    $day_update = Weekdays::find($day);
                    $dta['package_id'] = $id;
                    $day_update->update( $dta );
               }
          }

          return redirect()->route('admin.packages.index')->with('success', 'Paquete actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
          $packages = Packages::find($id);
          $packages->delete();
          return back()->with('success', 'Administrador eliminado');

     }
}
