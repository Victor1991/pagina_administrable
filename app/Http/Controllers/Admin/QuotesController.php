<?php

namespace App\Http\Controllers\Admin;

use App\Quotes;
use App\Packages;
use Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mail\QuotesEmail;
use App\Mail\QuotesUserEmail;
use App\Mail\FinishQuotesUserEmail;
use Illuminate\Support\Facades\Mail;

class QuotesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $users = User::all();
          return view('admin.calendar.index', compact('users'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $quotes = new Quotes;
          $packages = Packages::where('status', 1)->get();
          return view('admin.quotes.create', compact('quotes', 'packages'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
               'package_id' => 'required',
               'name' => 'required|string',
               'visit' => 'required|string',
               'direction' => 'required|string',
               'phone' => 'required|string',
               'email' => 'required|string|email',
               'direction' => 'required|string',
          ]);

          $data['visit'] = Carbon::parse($request->visit)->format('Y-m-d');
          $data['mail'] = $request->email;

          $quotes = Quotes::create($data);
          Mail::to("taydetidy@yahoo.com")->send(new QuotesEmail($quotes));
          Mail::to($request->email)->send(new QuotesUserEmail($quotes));

          return redirect()->route('admin.calendar.index')->with('success', 'Cita registrada');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {
          $quotes = Quotes::find($id);
          return Response::json($quotes->load('package'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $quotes = Quotes::find($id);
          $packages = Packages::where('status', 1)->get();

          return view('admin.quotes.edit', compact('quotes', 'packages'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {

          $quotes = Quotes::find($id);


          $data = $request->validate([
               'package_id' => 'required',
               'name' => 'required|string',
               'visit' => 'required|string',
               'direction' => 'required|string',
               'phone' => 'required|string',
               'email' => 'required|string|email',
               'direction' => 'required|string',
          ]);

          $data['visit'] = Carbon::parse($request->visit)->format('Y-m-d');
          $data['mail'] = $request->email;
          $data['hours'] = $request->hours;
          $data['cost'] = $request->cost;

          $quotes->update( $data );

          Mail::to("vic.hug.urib@gmail.com")->send(new QuotesEmail($quotes));
          Mail::to($request->email)->send(new QuotesUserEmail($quotes));
          return redirect()->route('admin.calendar.index')->with('success', 'Cita editada');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(User $user)
     {

     }

     public function register_appointment(Request $request)
     {
          $data = $request->validate([
               'package_id' => 'required',
               'name' => 'required|string',
               'visit' => 'required|string',
               'direction' => 'required|string',
               'phone' => 'required|string',
               'email' => 'required|string|email',
               'direction' => 'required|string',
          ]);

          $data['visit'] = Carbon::parse($request->visit)->format('Y-m-d');
          $data['mail'] = $request->email;
          $data['hours'] = $request->package_horas;
          $data['cost'] = $request->package_costo;

          $quotes = Quotes::create($data);
          Mail::to("vic.hug.urib@gmail.com")->send(new QuotesEmail($quotes));
          Mail::to($request->email)->send(new QuotesUserEmail($quotes));
          return Response::json($quotes);
     }

     public function finish_quote( $id )
     {
          $quote = Quotes::find($id);
          $data['finalized'] = 1;
          $quote->update( $data );
          Mail::to($quote->mail)->send(new FinishQuotesUserEmail($quote));

          // return Response::json($quote);

      }
}
