<?php

namespace App\Http\Controllers\Admin;

use App\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {

     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {

     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Configuration $configuration)
     {

     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $configuration = Configuration::find($id);
          return view('admin.configuration.edit', compact('configuration'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
          $configuration = Configuration::find($id);

          $data['facebook'] = request('facebook') ;
          $data['linkedin'] = request('linkedin');
          $data['instagram'] = request('instagram');
          $data['twitter'] = request('twitter');
          $data['youtube'] = request('youtube');
          $data['snapchat'] = request('snapchat');
          $data['mail'] = request('mail');
          $data['address'] = request('address');
          $data['phone'] = request('phone');

          $configuration->update( $data );
          return redirect()->route('admin.configuration.edit', ['configuration' => $id])->with('success', 'Configuraciones actualizadas');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(User $configuration)
     {
          $configuration->delete();
          return back()->with('success', 'Administrador eliminado');

     }
  }
