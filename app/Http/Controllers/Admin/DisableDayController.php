<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DisableDay;
use Response;


class DisableDayController extends Controller
{
    public function show($date)
    {
         $quotes = DisableDay::where('day',$date)->first();
         return Response::json($quotes);
    }

    public function store(Request $request)
    {
        $status = request('status') == 1 ? 1 : 0 ;
        if (request('fec_id')) {
            $date = DisableDay::find(request('fec_id'));
            $update['day'] = request('fec_dis');
            $update['status'] = $status;
            $date->update( $update );
        }else{
            $save['day'] = request('fec_dis');
            $save['status'] = $status;
            DisableDay::create($save);
           
        }

        return redirect()->back()->with('success', 'Exito al actualizar');
         
    }
}
