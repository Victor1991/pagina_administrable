<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\PageView;
use App\Quotes;
use App\DisableDay;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller{

     public function index(){
          $user = User::all();
          $page_views = PageView::orderBy('created_at', 'asc')->get();
          $quotes = Quotes::all();
          $disable_days = DisableDay::where('status', '1')->get();
          return view('admin.dashboard', compact('user', 'page_views', 'quotes', 'disable_days'));
     }

}
