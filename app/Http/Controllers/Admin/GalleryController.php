<?php

namespace App\Http\Controllers\Admin;

use App\Images;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $images = Images::all();
          return view('admin.gallery.index', compact('images'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $images = new Images;
          return view('admin.gallery.create', compact('images'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

          $data = $request->validate([
               'photo' => 'required'
          ]);

          $data['status'] = 1;

          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }

          $packages = Images::create($data)->id;

          return redirect()->route('admin.gallery.index')->with('success', 'Imagen registrada');
     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Packages $packages)
     {
          return view('admin.gallery.show', compact('packages'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $images = Images::find($id);
          return view('admin.gallery.edit', compact('images'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
          $imagen = Images::find($id);

          $data = $request->validate([
               'photo' => 'required'
          ]);

          $data['status'] = 1;


          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }

          $imagen->update( $data );

          return redirect()->route('admin.gallery.index')->with('success', 'Paquete actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
          $Images = Images::find($id);
          $Images->delete();
          return back()->with('success', 'Administrador eliminado');

     }
}
