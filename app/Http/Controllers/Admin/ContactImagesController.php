<?php

namespace App\Http\Controllers\Admin;

use App\ContactImages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class ContactImagesController extends Controller
{
     public function index()
    {
         $images = ContactImages::all();
         return view('admin.contact_gallery.index', compact('images'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
         $images = new ContactImages;
         return view('admin.contact_gallery.create', compact('images'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

         $data = $request->validate([
              'photo' => 'required'
         ]);

         $data['status'] = 1;

         if ($request->file('photo') != null) {
              $photo = $request->file('photo')->store('public');
              $data['photo'] = Storage::url($photo);
         }

         $packages = ContactImages::create($data)->id;

         return redirect()->route('admin.contact_images.index')->with('success', 'Imagen registrada');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Packages $packages)
    {
         return view('admin.contact_gallery.show', compact('packages'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
         $images = ContactImages::find($id);
         return view('admin.contact_gallery.edit', compact('images'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
         $imagen = ContactImages::find($id);

         $data = $request->validate([
              'photo' => 'required'
         ]);

         $data['status'] = 1;


         if ($request->file('photo') != null) {
              $photo = $request->file('photo')->store('public');
              $data['photo'] = Storage::url($photo);
         }

         $imagen->update( $data );

         return redirect()->route('admin.contact_images.index')->with('success', 'Paquete actualizado');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
         $ContactImages = ContactImages::find($id);
         $ContactImages->delete();
         return back()->with('success', 'Administrador eliminado');

    }
}
