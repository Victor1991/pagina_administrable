<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Testimonials;


class TestimonialsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $testimonials = Testimonials::all();
          return view('admin.testimonials.index', compact('testimonials'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
          $testimonial = new Testimonials;
          return view('admin.testimonials.create', compact('testimonial'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'description' => 'required|string',
               'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/'
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;

          if ($request->file('photo') != null) {
               $photo = $request->file('photo')->store('public');
               $data['photo'] = Storage::url($photo);
          }


          $packages = Packages::create($data)->id;

          if (request('tasks') ) {
               foreach (request('tasks') as $key => $tasks) {
                    $task['name'] = $tasks;
                    $task['package_id'] = $packages;
                    Tasks::create($task);
               }
          }



          return redirect()->route('admin.packages.index')->with('success', 'Paquete registrado');
     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Packages $packages)
     {
          return view('admin.packages.show', compact('packages'));
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $testimonial = Testimonials::find($id);
          return view('admin.testimonials.edit', compact('testimonial'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
          $testimonial = Testimonials::find($id);

          $data = $request->validate([
               'qualification' => 'required',
               'description' => 'required'
          ]);

          $data['on_page'] = request('on_page') == 1 ? 1 : 0 ;

          $testimonial->update( $data );

          return redirect()->route('admin.testimonials.index')->with('success', 'Testimonial actualizado');
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
          $packages = Packages::find($id);
          $packages->delete();
          return back()->with('success', 'Administrador eliminado');

     }
}
