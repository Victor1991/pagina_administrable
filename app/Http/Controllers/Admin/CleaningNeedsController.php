<?php

namespace App\Http\Controllers\Admin;

use App\CleaningNeeds;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CleaningNeedsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $cleaningneeds = CleaningNeeds::all();
          return view('admin.cleaning_needs.index', compact('cleaningneeds'));
     }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {

     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Packages $packages)
     {

     }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
          $cleaningneeds = CleaningNeeds::find($id);
          return view('admin.cleaning_needs.edit', compact('cleaningneeds'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
          $cleaningneeds = CleaningNeeds::find($id);

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'description' => 'required|string',
               'title' => 'required|string'
          ]);

          $data['status'] = request('status') == 1 ? 1 : 0 ;


          if ($request->file('icon') != null) {
               $photo = $request->file('icon')->store('public');
               $data['icon'] = Storage::url($photo);
          }

          if ($request->file('image') != null) {
               $photo = $request->file('image')->store('public');
               $data['image'] = Storage::url($photo);
          }

          $cleaningneeds->update( $data );

          return redirect()->route('admin.cleaning_need.index')->with('success', 'Paquete actualizado');
     }


}
