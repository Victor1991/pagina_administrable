<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PageView;
use Carbon\Carbon;
use App\Configuration;
use App\Packages;
use App\Images;
use App\Quotes;
use App\Testimonials;
use App\CleaningNeeds;
use App\ContactImages;
use App\DisableDay;
use App\Weekdays;

class PageController extends Controller
{

     public function index()
     {
          $data['data'] = $_SERVER['HTTP_USER_AGENT'];
          PageView::create($data);
          $configuration = Configuration::find(1);
          $packages = Packages::where('status', 1)->get();
          $testimonials = Testimonials::where('on_page', 1)->get();
          $galery = Images::where('contact', 0)->where('status', 1)->get();
          $cleaningneeds = CleaningNeeds::all();
          $contactimages = ContactImages::all();
          $disable_days = DisableDay::where('status', '1')->get();
          $wkdy = Weekdays::all();

          return view('welcome', compact('configuration', 'packages','galery', 'cleaningneeds', 'contactimages', 'testimonials', 'disable_days', 'wkdy'));
     }

     public function data_view_json()
     {
          $data = PageView::selectRaw("COUNT(*) views, DATE_FORMAT(created_at, '%Y-%m-%e %H:%i:00') date")
          ->groupBy('date')
          ->get();

          $view = array();
          foreach ($data as $key => $value) {
               // $view[$key][] = strtotime($value['date']) * 1000;
               $carbon_obj = Carbon::createFromFormat('Y-m-d H:i:s' , $value['date'],'America/Mexico_City');
               $view[$key][] = (int)round($carbon_obj->format('Uu') / pow(10, 6 - 3));
               $view[$key][] = $value['views'];
          }

          return response()->json($view);
     }

     public function testimonials($id)
     {
          $quotes = Quotes::find($id);
          return view('testimonial', compact('quotes'));
     }

     public function save_testimonial(Request $request)
     {
          $data = $request->validate([
               'qualification' => 'required',
               'description' => 'required'
          ]);

          $data['quote_id'] = request('id') ;

          $packages = Testimonials::create($data);
          return redirect()->route('testimonials', request('id')  )->with('success', 'Paquete actualizado');

     }

}
