<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
     //
     protected $fillable = [
          'name', 'description', 'price', 'foto', 'status', 'photo'
     ];

     public function tasks()
     {
          return $this->hasMany(Tasks::class, 'package_id', 'id');
     }

     public function weekdays()
     {
          return $this->hasMany(Weekdays::class, 'package_id', 'id');
     }
}
