<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--
Responsive Email Template by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
-->

<div id="mailsub" class="notification" align="center">

     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


          <!--[if gte mso 10]>
          <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr><td>
          <![endif]-->

          <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
               <tr>
                    <td>
                         <!-- padding -->
                         <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
                    </td>
               </tr>
               <!--header -->
               <tr>
                    <td align="center" bgcolor="#ffffff">
                         <!-- padding -->
                         <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
                         <table width="90%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                   <td align="left"><!--Item -->
                                        <div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
                                             <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
                                                  <tr>
                                                       <td align="left" valign="middle">
                                                            <!-- padding -->
                                                            <div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                                                            <table width="115" border="0" cellspacing="0" cellpadding="0" >
                                                                 <tr>
                                                                      <td align="left" valign="top" class="mob_center">
                                                                           <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                                     <img src="{{ asset('/img/logo.png') }}" width="115" height="19" alt="TAYDE TIDY Cleaning Services" border="0" style="display: block;" />
                                                                                </font>
                                                                           </a>
                                                                      </td>
                                                                 </tr>
                                                            </table>
                                                       </td>
                                                  </tr>
                                             </table>
                                        </div>
                                        <!-- Item END--><!--[if gte mso 10]>
                                   </td>
                                   <td align="right">
                                   <![endif]--><!--Item -->
                                   <!-- <div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
                                        <table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
                                             <tr>
                                                  <td align="right" valign="middle">
                                                       <div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                                                       <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                            <tr>
                                                                 <td align="right">
                                                                      <div class="mob_center_bl" style="width: 88px;">
                                                                           <table border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                     <td width="30" align="center" style="line-height: 19px;">
                                                                                          <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                               <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                                    <img src="http://artloglab.com/metromail/images/facebook.gif" width="10" height="19" alt="Facebook" border="0" style="display: block;" />
                                                                                               </font>
                                                                                          </a>
                                                                                     </td>
                                                                                     <td width="39" align="center" style="line-height: 19px;">
                                                                                          <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                               <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                                    <img src="http://artloglab.com/metromail/images/twitter.gif" width="19" height="16" alt="Twitter" border="0" style="display: block;" />
                                                                                               </font>
                                                                                          </a>
                                                                                     </td>
                                                                                     <td width="29" align="right" style="line-height: 19px;">
                                                                                          <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                               <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                                    <img src="http://artloglab.com/metromail/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;" />
                                                                                               </font>
                                                                                          </a>
                                                                                     </td>
                                                                                </tr>
                                                                           </table>
                                                                      </div>
                                                                 </td>
                                                            </tr>
                                                       </table>
                                                  </td>
                                             </tr>
                                        </table>
                                   </div> -->
                                   <!-- Item END-->
                              </td>
                         </tr>
                    </table>
                    <!-- padding -->
                    <div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
               </td>
          </tr>
          <!--header END-->

          <!--content 1 -->
          <tr>
               <td align="center" bgcolor="#fbfcfd">
                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                              <td align="center">
                                   <!-- padding -->
                                   <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                                   <div style="line-height: 44px;">
                                        <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                                             <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                                  Finished service
                                             </span>
                                        </font>
                                   </div>
                                   <!-- padding -->
                                   <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                              </td>
                         </tr>
                         <tr>
                              <td align="center">
                                   <div style="line-height: 24px;">
                                        <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                             <span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                  <p>
                                                       Hi {{ $quotes->name }}, we would like you to share your testimony about the service, it would not help much to continue improving
                                                  </p>
                                             </span>
                                        </font>
                                   </div>
                                   <!-- padding -->
                                   <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                              </td>
                         </tr>
                         <tr>
                              <td align="center">
                                   <div style="line-height: 24px;">
                                        <a href="{{ route('testimonials', $quotes->id) }}" target="_blank" style="background-color:#44c767; border-radius:9px; border:1px solid #18ab29; display:inline-block; cursor:pointer; color:#ffffff; font-family:Arial; font-size:15px; padding:8px 60px; text-decoration:none; text-shadow:0px -1px 0px #2f6627;">
                                              testify
                                        </a>
                                   </div>
                                   <!-- padding -->
                                   <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                              </td>
                         </tr>
                    </table>
               </td>
          </tr>
          <!--content 1 END-->


          <!--footer -->
          <tr>
               <td class="iage_footer" align="center" bgcolor="#ffffff">
                    <!-- padding -->
                    <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                              <td align="center">
                                   <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                             <?=date('Y')?> © <a href="http://monkeystudio.xyz/">monkeystudio.xyz</a>. ALL Rights Reserved.
                                        </span>
                                   </font>
                              </td>
                         </tr>
                    </table>

                    <!-- padding -->
                    <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
               </td>
          </tr>
          <!--footer END-->
          <tr>
               <td>
                    <!-- padding -->
                    <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
               </td>
          </tr>
     </table>
     <!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->

</td>
</tr>
</table>

</div>
