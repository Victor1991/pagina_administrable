<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

     <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta http-equiv="content-type" content="text/html; charset=utf-8" />
     <meta name="author" content="MonkeyStudio" />

     <!-- Stylesheets
     ============================================= -->
     <link href="http://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/bootstrap.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/style.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/dark.css')}}" type="text/css" />

     <link rel="stylesheet" href="{{ asset('/pagina/css/font-icons.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/animate.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/magnific-popup.css')}}" type="text/css" />

     <link rel="stylesheet" href="{{ asset('/pagina/css/responsive.css')}}" type="text/css" />
     <meta name="viewport" content="width=device-width, initial-scale=1" />

     <!-- Workspace Demo Specific Stylesheet -->
     <link rel="stylesheet" href="{{ asset('/pagina/css/colors.php?color=267DF4')}}" type="text/css" /> <!-- Theme Color -->
     <link rel="stylesheet" href="{{ asset('/pagina/demos/coworking/css/fonts.css')}}" type="text/css" /> <!-- Theme Font -->
     <link rel="stylesheet" href="{{ asset('/pagina/demos/coworking/coworking.css')}}" type="text/css" /> <!-- Theme CSS -->
     <!-- / -->

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
     <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
     <!-- Document Title
     ============================================= -->
     <title>TAYDE TIDY Cleaning Services</title>

</head>

<body class="stretched">
     <div class="container">

          <div class="row justify-content-center" style="margin-bottom:50px">
               <?php if ($quotes->testimonial): ?>
                    <div class=" bottommargin-lg dark" style="background: url( https://sistetema.monkeystudio.xyz/pagina/img/slide2.jpg ) center 80%; background-size: cover; padding: 180px 0">
                         <div class="container clearfix">
                              <div class="emphasis-title divcenter center" style="max-width: 700px">
                                   <h2 class="display-4 t600 ls--2"> Thanks for your testimony <span class="text-rotater" data-separator="|" data-rotate="fadeIn" data-speed="2000">
                                   <br>
                                   <a style="font-size: 28px;" href="{{ url('/') }}">Return home </a>
                              </div>
                         </div>
                    </div>
               <?php else: ?>
                    <div class="col-md-7">
                         <h2 class="nott t600" style="font-size:42px">Testimonial</h2>
                         <p class="text-dark">Hi {{ $quotes->name }}, we would like you to share your testimony about the service, it would not help much to continue improving</p>

                         <div class="form-widget">

                              <div class="form-result">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                              <form  name="template-contactform" action="{{ route('save_testimonial') }}" method="post">
                                   {{ csrf_field() }}
                                   <input type="hidden" name="id" value="{{$quotes->id}}">

                                   <div class="form-process">
                                        <div class="css3-spinner">
                                             <div class="css3-spinner-scaler"></div>
                                        </div>
                                   </div>

                                   <div class="row">
                                        <div class="col-md-12 form-group">
                                             <label for="template-contactform-message">Qualification<small class="text-danger">*</small></label><br>

                                             <p class="clasificacion" style="margin-bottom: 5px;">
                                                  <input id="radio1" type="radio" name="qualification" value="5" checked>
                                                  <label for="radio1">★</label>
                                                  <input id="radio2" type="radio" name="qualification" value="4">
                                                  <label for="radio2">★</label>
                                                  <input id="radio3" type="radio" name="qualification" value="3">
                                                  <label for="radio3">★</label>
                                                  <input id="radio4" type="radio" name="qualification" value="2">
                                                  <label for="radio4">★</label>
                                                  <input id="radio5" type="radio" name="qualification" value="1">
                                                  <label for="radio5">★</label>
                                             </p>
                                        </div>

                                        <div class="w-100"></div>

                                        <div class="col-12 form-group">
                                             <label for="template-contactform-message">Message<small class="text-danger">*</small></label>
                                             <textarea class="required form-control" id="template-contactform-message" name="description" rows="6" cols="30"></textarea>
                                        </div>

                                        <div class="col-12 form-group">
                                             <button class="button button-rounded button-xlarge m-0 center font-weight-medium nott ls0 font-weight-normal" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                        </div>
                                   </div>

                                   <input type="hidden" name="prefix" value="template-contactform-">

                              </form>
                         </div>
                    </div>
               <?php endif; ?>
          </div>
     </div>

     <style>
     p.clasificacion {
          position: relative;
          overflow: hidden;
          display: inline-block;
     }

     p.clasificacion input {
          position: absolute;
          top: -100px;
     }

     p.clasificacion label {
          float: right;
          color: #333;
          font-size: 30px;
     }

     p.clasificacion label:hover,
     p.clasificacion label:hover ~ label,
     p.clasificacion input:checked ~ label {
          color: #dd4;
     }
     </style>

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
