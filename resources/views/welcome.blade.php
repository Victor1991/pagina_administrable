<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

     <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta http-equiv="content-type" content="text/html; charset=utf-8" />
     <meta name="author" content="MonkeyStudio" />

     <!-- Stylesheets
     ============================================= -->
     <link href="http://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/bootstrap.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/style.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/dark.css')}}" type="text/css" />

     <link rel="stylesheet" href="{{ asset('/pagina/css/font-icons.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/animate.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{ asset('/pagina/css/magnific-popup.css')}}" type="text/css" />

     <link rel="stylesheet" href="{{ asset('/pagina/css/responsive.css')}}" type="text/css" />
     <meta name="viewport" content="width=device-width, initial-scale=1" />

     <!-- Workspace Demo Specific Stylesheet -->
     <link rel="stylesheet" href="{{ asset('/pagina/css/colors.php?color=267DF4')}}" type="text/css" /> <!-- Theme Color -->
     <link rel="stylesheet" href="{{ asset('/pagina/demos/coworking/css/fonts.css')}}" type="text/css" /> <!-- Theme Font -->
     <link rel="stylesheet" href="{{ asset('/pagina/demos/coworking/coworking.css')}}" type="text/css" /> <!-- Theme CSS -->
     <!-- / -->

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
     <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
     <!-- Document Title
     ============================================= -->
     <title>TAYDE TIDY Cleaning Services</title>

</head>

<body class="stretched">

     <!-- Document Wrapper
     ============================================= -->
     <div id="wrapper" class="clearfix">

          <!-- Header
          ============================================= -->
          <header id="header" class="clearfix static-sticky border-bottom-0">

               <div id="header-wrap" style="background-color:#1a3a60">

                    <div class="container clearfix">

                         <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                         <nav id="primary-menu">

                              <ul>
                                   <li class="current"><a href="#"><div>Home</div></a></li>
                                   <li><a href="#about"><div>About Us</div></a></li>
                                   <li><a href="#rates"><div>Rates</div></a></li>
                                   <li><a href="#contact"><div>Contact Us</div></a></li>
                              </ul>

                              <!-- Top Search
                              ============================================= -->
                              <div id="top-account">
                                   <a href="tel:{{$configuration->phone}}"><i class="icon-call"></i> <span>{{$configuration->phone}}</span></a>
                              </div><!-- #top-search end -->

                         </nav><!-- #primary-menu end -->
                         <!-- Logo
                         ============================================= -->
                         <div id="logo">
                              <!-- <a href="index.html" class="standard-logo" data-sticky-logo="{{ asset('/pagina/img/logo.png') }}"><img src="{{ asset('/pagina/img/logo.png') }}" alt="TAYDE TIDY"></a>
                              <a href="index.html" class="retina-logo" data-sticky-logo="{{ asset('/pagina/img/logo.png') }}"><img src="{{ asset('/pagina/img/logo.png') }}" alt="TAYDE TIDY"></a> -->
                         </div>
                         <!-- #logo end -->

                         <!-- Primary Navigation
                         ============================================= -->


                    </div>

               </div>

          </header><!-- #header end -->

          <!-- Content
          ============================================= -->
          <section id="slider" class="slider-element slider-parallax swiper_wrapper full-screen clearfix">
               <div class="slider-parallax-inner">
                    <div class="swiper-container swiper-parent">
                         <div class="swiper-wrapper">
                              <div class="swiper-slide dark" style="background-image: url({{ asset('pagina/img/slide10.jpg') }});">
                                   <div class="container clearfix">
                                        <div class="container full-screen position-relative">
                                             <div class="slider-typography text-center">
                                                  <div class="slider-text-middle-main">
                                                       <div class="slider-text-middle slider-typography-option1 ">
                                                            <div class="row">
                                                                 <div class="col-md-12">
                                                                      <img src="{{ asset('pagina/img/slide3.png')}}" class="xs-width-70" alt=""/>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                       <a href="#contact" class="button button-rounded button-large nott ls0 font-primary">Make a Quote</a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>
                         </div>
                    </div>
               </div>
          </section>

          <!-- Content
          ============================================= -->
          <section id="content">
               <div class="content-wrap pt-3 pb-0">
                    <div class="section nobg py-2">
                         <div class="container">
                              <div class="row">
                                   <div class="col-md-7">
                                        <h2 class="display-4 t600 ls--2">We are making better <span class="text-rotater" data-separator="|" data-rotate="fadeIn" data-speed="2000">
                                             <span class="t-rotate">Cleaning|Trustworthy |Best services|Faster </span>
                                        </span> for you.</h2>
                                   </div>
                                   <div class="col-md-5">
                                        <p class="lead text-muted">
                                             Tayde Tidy is a company dedicated to the field of <strong>cleaning for 10 years,</strong> we seek quality in our services and achieve the trust of our client.
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="margen mt-0 pt-md-5 pt-0" id="rates">
                         <div class="divcenter" style="max-width: 960px">
                              <div class="heading-block mb-4 nobottomborder">
                                   <div class="before-heading">Know More</div>
                                   <h2 class="nott t600">Let us take care of your cleaning needs</h2>
                              </div>
                              <div class="tabs tabs-alt tabs-responsive tabs-justify clearfix" id="tab">
                                   <ul class="tab-nav clearfix">
                                        <?php foreach ($cleaningneeds as $key => $cleaningneed): ?>
                                             <li>
                                                  <a href="#tabs-<?=$cleaningneed->id?>" style="background-color:#f4f8fe">
                                                       <center>
                                                            <span class="iconbox-icon-container">
                                                                 <img src="{{ $cleaningneed->icon }}" class="liquid-image-icon" alt="Icon-{{ $cleaningneed->name }}" style="padding-bottom:10px">
                                                            </span>
                                                       </center>
                                                       {{ $cleaningneed->name }}
                                                  </a>
                                             </li>
                                        <?php endforeach; ?>
                                   </ul>

                                   <div class="tab-container">
                                        <?php $ind = 1; ?>
                                        <?php foreach ($cleaningneeds as $key => $cleaningneed): ?>
                                             <div class="tab-content clearfix" id="tabs-{{$cleaningneed->id}}">
                                                  <div class="story-box <?php if ($ind % 2 == 0): ?> description-left <?php endif; ?>  clearfix">
                                                       <div class="story-box-image">
                                                            <img src="{{ $cleaningneed->image }}" alt="img-{{ $cleaningneed->name }}" alt="story-image">
                                                       </div>
                                                       <div class="story-box-info">
                                                            <h3 class="story-title">{{ $cleaningneed->title }}</h3>
                                                            <div class="story-box-content">
                                                                 <p>{{ $cleaningneed->description }}</p>
                                                                 <a href="#contact" class="t300 button noleftmargin button-rounded" style="text-decoration: none !important;">Make a quote</a>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                             <?php $ind++; ?>
                                        <?php endforeach; ?>

                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class=" bottommargin-lg dark" style="background: url( {{ asset('pagina/img/slide2.jpg')}} ) center 80%; background-size: cover; padding: 180px 0">
                         <div class="container clearfix">
                              <div class="emphasis-title divcenter center" style="max-width: 700px">
                                   <h2 class="display-4 t600 ls--2">We are making better <span class="text-rotater" data-separator="|" data-rotate="fadeIn" data-speed="2000">
                                        <span class="t-rotate">Cleaning|Trustworthy |Best services|Faster </span>
                                   </span> for you.</h2>
                              </div>
                         </div>
                    </div>

                    <div  id="section-tickets" class="section page-section nobg pt-0 clearfix">


                         <div class="container">
						<div class="mb-5 dark d-flex justify-content-between align-items-center">
							<h2 class="display-4 t600 ls--2" style="color: #000;">Let us take care of your<br><span class="t-rotate"> cleaning needs</span>

 <span class="text-rotater" data-separator="|" data-rotate="fadeIn" data-speed="2000">

							</span> </h2>
						</div>
					</div>

                         <div class="container">
                              <div class="row  align-items-center clearfix">

                                   <?php foreach ($packages as $key => $package): ?>
                                        <div class="col-lg-4 col-sm-12 mt-4 mt-sm-0">

                                             <div class="price-list shadow-sm card rounded  ">
                                                  <div class="position-relative">
                                                       <?php if ($package->photo): ?>
                                                            <img src="{{ $package->photo }}" alt="Featured image 1" class="card-img-top rounded-top">
                                                       <?php else: ?>
                                                            <img src="{{ asset('/pagina/img/img1.jpg')}}" alt="Featured image 1" class="card-img-top rounded-top">
                                                       <?php endif; ?>
                                                       <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                                                            <h3 class="card-title mb-0 text-white">{{ $package->name }}</h3>
                                                            <p class="card-text mb-0">Price per hour of services</p>
                                                       </div>
                                                  </div>
                                                  <div class="card-body">

                                                       <div class="pricing-price 1-day-pricing font-primary t700 text-center" >
                                                            <span class="price-unit">&dollar;</span> <strong  id="pri_selec_package_{{$package->id}}" >{{ $package->price * 4 }}</strong>
                                                       </div>
                                                       <div class="Pricing-persons">
                                                            <select class="required custom-select" name="1-day-pricing-person" id="selec_package_{{$package->id}}" onchange="cambiar_precio(this)">
                                                                 <?php for ($i=4; $i <= 12; $i++) {
                                                                      echo "<option data-cost='".$package->price * $i."' value='".$i."'>".$i." Hour</option>";
                                                                 } ?>

                                                            </select>
                                                       </div>
                                                       <br>
                                                       <button href="#" onclick="solicitar(<?=$package->id?>)" class="button button-rounded button-large btn-block m-0 center t500">Request service</button>

                                                  </div>
                                             </div>
                                        </div>
                                   <?php endforeach; ?>
                              </div>

                         </div>
                    </div>
               </div>

               <div class="section clearfix nobg" style="margin-top: -70px;">
                    <div class="container">
                         <div class="heading-block nobottomborder center">
                              <div class="before-heading">Some Of Gallery Pictures</div>
                              <h2 class="nott t600 mb-0">Gallery</h2>
                         </div>
                    </div>
                    <div id="image-carousel" class="owl-carousel carousel-widget" data-margin="24" data-nav="true" data-pagi="true" data-items-xs="1" data-items-sm="1" data-items-md="1" data-items-lg="2" data-items-xl="2" style="padding: 0 20px" data-lightbox="gallery">
                         <div class="slide slide--1">
                              <div class="carousel-column-container">
                                   <ul class="carousel-column column-1">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[0]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[0]->photo }});"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[1]->photo}}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[1]->photo}});"></a>
                                        </li>
                                   </ul>
                                   <ul class="carousel-column column-2">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[2]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[2]->photo }}); min-height: 300px"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[3]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[3]->photo }}); min-height: 280px"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[4]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[4]->photo }}); min-height: 200px"></a>
                                        </li>
                                   </ul>
                              </div>
                         </div>

                         <div class="slide slide--2">
                              <div class="carousel-column-container">
                                   <ul class="carousel-column column-1">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[5]->photo}}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[5]->photo}}); min-height: 324px"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <div class="item bgcolor d-flex align-items-center px-4" style="min-height: 480px">
                                                  <blockquote class="blockquote border-0 mb-0">
                                                       <p class="mb-3 text-white">"The Spirit of CoWorking Allows You to Find CoWorkers Who're Worth Working with."</p>
                                                       <footer class="blockquote-footer text-white-50 font-italic">Cynthia Chiam</footer>
                                                  </blockquote>
                                             </div>
                                        </li>
                                   </ul>
                                   <ul class="carousel-column column-2">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[6]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[6]->photo }});"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[7]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[7]->photo }});"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[8]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[8]->photo }});"></a>
                                        </li>
                                   </ul>
                              </div>
                         </div>

                         <div class="slide slide--3">
                              <div class="carousel-column-container">
                                   <ul class="carousel-column column-1">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[9]->photo }}" data-lightbox="gallery-item" class="item bgcolor d-flex align-items-center px-4 img-overlay" style="background-image: url({{ $galery[9]->photo }}); min-height: 350px">
                                                  <blockquote class="blockquote border-0 mb-0">
                                                       <p class="mb-3 text-white">"I Belive in Collaboration Rather Than Competition"</p>
                                                       <footer class="blockquote-footer text-white-50 font-italic">Cynthia Chiam</footer>
                                                  </blockquote>
                                             </a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[10]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[10]->photo }}); min-height: 453px"></a>
                                        </li>
                                   </ul>
                                   <ul class="carousel-column column-2">
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[11]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[11]->photo }});"></a>
                                        </li>
                                        <li class="carousel-grid-item">
                                             <a href="{{ $galery[12]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[12]->photo }});"></a>
                                        </li>
                                        <li class="carousel-grid-item">

                                             <a href="{{ $galery[13]->photo }}" data-lightbox="gallery-item" class="item" style="background-image: url({{ $galery[13]->photo }});"></a>
                                        </li>
                                   </ul>
                              </div>
                         </div>
                    </div>

               </div>

               <div class="section p-0 testimonials clearfix">
                    <div class="container" style="padding: 120px 0">
                         <div class="heading-block nobottomborder center">
                              <div class="before-heading">What our Members Say</div>
                              <h2 class="nott t600">Testimonials</h2>
                         </div>

                         <div id="testimonials-carousel" class="owl-carousel carousel-widget testimonial testimonial-full nobgcolor noborder noshadow nopadding divcenter tleft clearfix" data-animate-in="fadeIn" data-animate-out="fadeOut" data-margin="24" data-nav="false" data-pagi="true" data-items="1" style="max-width: 740px">
                              <?php foreach ($testimonials as $key => $testimonial): ?>
                                   <div class="slide">
                                        <div class="testi-content">
                                             <div class="testi-stars mb-4">
                                                  <?php for ($i=0; $i < $testimonial->qualification ; $i++) {
                                                       echo '<i class="icon-star3" style="padding: 5px; margin: 2px;"></i>';
                                                  } ?>

                                             </div>
                                             <p>{{ $testimonial->description }}</p>
                                             <div class="testi-meta mt-4">
                                                  {{ $testimonial->quote->name }}
                                             </div>
                                        </div>
                                   </div>
                              <?php endforeach; ?>
                         </div>
                    </div>

               </div>

               <div class="section p-0 nobg clearfix">
                    <div class="container">
                         <div class="row mb-2">
                              <div class="col-lg-6 col-md-4">
                                   <div class="heading-block nobottomborder mb-4">
                                        <div class="before-heading">How do you Contact Us</div>
                                        <h2 class="nott t600">Contact</h2>
                                   </div>
                              </div>

                              <div class="col-lg-6 col-md-8">
                                   <div class="row">
                                        <div class="col-sm-4 col-3">
                                             <div class="counter color t600"><span data-from="1" data-to="3" data-refresh-interval="2" data-speed="600"></span>+</div>
                                             <h5 class="mt-0 t500">Branches</h5>
                                        </div>

                                        <div class="col-sm-4 col-6">
                                             <div class="counter color t600"><span data-from="100" data-to="4500" data-refresh-interval="100" data-speed="1500"></span>+</div>
                                             <h5 class="mt-0 t500">Active Members</h5>
                                        </div>

                                        <div class="col-sm-4 col-3">
                                             <div class="counter color t600"><span data-from="2" data-to="100" data-refresh-interval="30" data-speed="1500"></span>%</div>
                                             <h5 class="mt-0 t500">Secured</h5>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div id="locations-carousel" class="owl-carousel carousel-widget owl-carousel-full clearfix" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="200" data-margin="0" data-nav="true" data-pagi="true" data-items="1">
                         <div>
                              <div class="masonry-thumbs grid-5" data-big="2" data-lightbox="gallery">
                                   <a href="{{ $contactimages[0]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[0]->photo }}" alt="Gallery Thumb 1"></a>
                                   <a href="#"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835253572242!2d144.95373531531297!3d-37.817327679751735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1554124945593!5m2!1sen!2sin" width="500" height="334" allowfullscreen  style="border:0;"></iframe></a>
                                   <a href="{{ $contactimages[1]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[1]->photo }}" alt="Gallery Thumb 12"></a>
                                   <a href="{{ $contactimages[2]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[2]->photo }}" alt="Gallery Thumb 3"></a>
                                   <a href="{{ $contactimages[3]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[3]->photo }}" alt="Gallery Thumb 4"></a>
                                   <a href="{{ $contactimages[4]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[4]->photo }}" alt="Gallery Thumb 5"></a>
                                   <a href="{{ $contactimages[5]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[5]->photo }}" alt="Gallery Thumb 6"></a>
                                   <a href="{{ $contactimages[6]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[6]->photo }}" alt="Gallery Thumb 7"></a>
                                   <a href="{{ $contactimages[7]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[7]->photo }}" alt="Gallery Thumb 13"></a>
                                   <a href="{{ $contactimages[8]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[8]->photo }}" alt="Gallery Thumb 6"></a>
                                   <a href="{{ $contactimages[9]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[9]->photo }}" alt="Gallery Thumb 7"></a>
                                   <a href="{{ $contactimages[10]->photo }}" data-lightbox="gallery-item"><img src="{{ $contactimages[10]->photo }}" alt="Gallery Thumb 12"></a>
                              </div>
                              <div class="container">
                                   <div class="card shadow bg-white">
                                        <div class="card-body">
                                             <h3 class="mb-4">Vancouver</h3>
                                             <div style="font-size: 16px;">
                                                  <address>
                                                       ST 5061 EARLES <br>Vancouver BC,<br>V5R 3R8
                                                  </address>
                                                  <div class="mb-4" style="font-size: 15px;">
                                                       <small class="text-black-50 d-block mb-2">Phone Number:</small>
                                                       <a href="tel:7786860095" class="text-dark d-block mb-1">77 8686-0095</a>
                                                       <a href="tel:7786860095" class="text-dark d-block mb-1">77 8686-0095</a>
                                                  </div>

                                                  <a href="#" class="button button-rounded m-0 nott t600 ls0">
                                                       Send us a WhatsApp</a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>



                    <div class="row justify-content-center" style="margin-bottom:50px">
                         <div class="col-md-7" id="contact">
                              <h2 class="nott t600" style="font-size:42px">Contact Us</h2>
                              <p class="text-dark">We are happy to help you, write your needs here. We will get in touch as soon as possible.</p>

                              <div class="form-widget topmargin">

                                   <div class="form-result"></div>

                                   <form class="mb-0" id="template-contactform" name="template-contactform" action="{{ asset('pagina/include/form.php') }}" method="post">

                                        <div class="form-process">
                                             <div class="css3-spinner">
                                                  <div class="css3-spinner-scaler"></div>
                                             </div>
                                        </div>

                                        <div class="row">
                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-name">Name<small class="text-danger">*</small></label>
                                                  <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="form-control required" />
                                             </div>

                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-email">Email<small class="text-danger">*</small></label>
                                                  <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email form-control" />
                                             </div>

                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-phone">Phone<small class="text-danger">*</small></label>
                                                  <input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="required form-control" />
                                             </div>

                                             <div class="w-100"></div>

                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-company">Company</label>
                                                  <input type="text" id="template-contactform-company" name="company" value="" class="form-control" />
                                             </div>

                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-workspace">Service<small class="text-danger">*</small></label>
                                                  <select id="template-contactform-workspace" name="template-contactform-workspace" class="required form-control">
                                                       <option value="" disabled="disabled" selected>-- Select One --</option>
                                                       <option value="Home">Home</option>
                                                       <option value="Business">Business</option>
                                                       <option value="Mall">Mall</option>
                                                       <option value="Colleges">Colleges</option>
                                                       <option value="General Cleaning">General Cleaning</option>
                                                  </select>
                                             </div>

                                             <div class="col-md-4 form-group">
                                                  <label for="template-contactform-service">Interested in<small class="text-danger">*</small></label>
                                                  <select id="template-contactform-service" name="template-contactform-service" class="required form-control">
                                                       <option value="" disabled="disabled" selected>-- Select One --</option>
                                                       <option value="Monday, Tuesday">Monday, Tuesday</option>
                                                       <option value="Wednesday, Thursday, Friday & Saturday">Wednesday, Thursday, Friday & Saturday</option>
                                                       <option value="Sunday">Sunday</option>
                                                  </select>
                                             </div>

                                             <div class="w-100"></div>

                                             <div class="col-12 form-group">
                                                  <label for="template-contactform-message">Message<small class="text-danger">*</small></label>
                                                  <textarea class="required form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                                             </div>

                                             <div class="col-12 form-group d-none">
                                                  <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="form-control" />
                                             </div>

                                             <div class="col-12 form-group">
                                                  <button class="button button-rounded button-xlarge m-0 center font-weight-medium nott ls0 font-weight-normal" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                             </div>
                                        </div>

                                        <input type="hidden" name="prefix" value="template-contactform-">

                                   </form>
                              </div>
                         </div>
                    </div>

               </div>

          </section><!-- #content end -->


          <!-- Footer
          ============================================= -->
          <footer id="footer" class="dark">

               <!-- Copyrights
               ============================================= -->
               <div id="copyrights">

                    <div class="container clearfix">

                         <div class="col_half">
                              <img src="{{ asset('pagina/img/logo-footer.png') }}" alt="" class="footer-logo" style="width:220px">
                         </div>

                         <div class="col_half col_last tright">
                              <div class=" copyright-links fright clearfix">
                                   <a href="{{ asset('/pdf/terms.pdf')}}" target="_blank">Notice Of Privacy</a><br>
                                   <span>Copyrights &copy; <?=date('Y')?> All Rights Reserved by TAYDE TIDY Cleaning Services.</span><br>
                                   <a href="https://monkeystudio.xyz/">Developed by MONKEYSTUDIO</a>
                              </div>
                              <div class="fright clearfix">
                                   <?php if ($configuration->facebook): ?>
                                        <a href="{{ $configuration->facebook }}" target="_blank" class="social-icon si-small si-borderless nobottommargin si-facebook">
                                             <i class="icon-facebook"></i>
                                             <i class="icon-facebook"></i>
                                        </a>
                                   <?php endif; ?>

                                   <?php if ($configuration->linkedin): ?>
                                        <a href="{{ $configuration->linkedin }}" target="_blank" class="social-icon si-small si-borderless nobottommargin si-linkedin">
                                             <i class="icon-linkedin"></i>
                                             <i class="icon-linkedin"></i>
                                        </a>
                                   <?php endif; ?>

                                   <?php if ($configuration->instagram): ?>
                                        <a href="{{ $configuration->instagram }}" target="_blank" class="social-icon si-small si-borderless nobottommargin si-instagram">
                                             <i class="icon-instagram"></i>
                                             <i class="icon-instagram"></i>
                                        </a>
                                   <?php endif; ?>

                                   <?php if ($configuration->twitter): ?>
                                        <a href="{{ $configuration->twitter }}" class="social-icon si-small si-borderless nobottommargin si-twitter">
                                             <i class="icon-twitter"></i>
                                             <i class="icon-twitter"></i>
                                        </a>
                                   <?php endif; ?>

                                   <?php if ($configuration->youtube): ?>
                                        <a href="{{ $configuration->youtube }}" class="social-icon si-small si-borderless nobottommargin si-youtube">
                                             <i class="icon-youtube"></i>
                                             <i class="icon-youtube"></i>
                                        </a>
                                   <?php endif; ?>

                                   <?php if ($configuration->snapchat): ?>
                                        <a href="{{ $configuration->snapchat }}" class="social-icon si-small si-borderless nobottommargin si-dwolla">
                                             <i class="icon-snapchat"></i>
                                             <i class="icon-snapchat"></i>
                                        </a>
                                   <?php endif; ?>



                              </div>
                         </div>

                    </div>

               </div><!-- #copyrights end -->

          </footer><!-- #footer end -->

     </div><!-- #wrapper end -->


     <!-- Modal -->
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header" style="background-color: #1a3a60;border-top-left-radius: 0px; border-top-right-radius: 0px;">
                         <h5 class="modal-title" style="color: #fff;">SCHEDULE YOUR APPOINTMENT</h5>
                    </div>
                    <form class="" id="groupForm" action="" method="post" style="margin-bottom: 0px;">
                         <div class="modal-body">
                              <div class="row">
                                   <div class="col-md-12">
                                        <input type="hidden" name="package_id" id="package_id" value="">
                                        <input type="hidden" name="package_horas" id="package_horas" value="">
                                        <input type="hidden" name="package_costo" id="package_costo" value="">
                                        <div class="form-group">
                                             <label for="exampleInputEmail1">Name</label>
                                             <input type="text" name="name" class="form-control" required >
                                        </div>
                                        <div class="form-group">
                                             <label for="exampleInputEmail1">Phone</label>
                                             <input type="text" name="phone" class="form-control" required >
                                        </div>
                                        <div class="form-group">
                                             <label for="exampleInputEmail1">Email</label>
                                             <input type="email" name="email" class="form-control" required >
                                        </div>
                                        <div class="form-group">
                                             <label for="exampleInputEmail1">Apoiment</label>
                                             <input type="text" name="visit" id="piker" class="form-control" required >
                                        </div>
                                        <div class="form-group">
                                             <label for="exampleInputEmail1">Adress</label>
                                             <textarea name="direction" class="form-control" rows="4" cols="80" required></textarea>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Send</button>
                         </div>
                    </form>
               </div>
          </div>
     </div>


     <!-- Go To Top
     ============================================= -->
     <div id="gotoTop" class="icon-angle-up"></div>

     <!-- External JavaScripts
     ============================================= -->
     <script src="{{ asset('/pagina/js/jquery.js')}}"></script>
     <script src="{{ asset('/pagina/js/plugins.js')}}"></script>

     <!-- Footer Scripts
     ============================================= -->
     <script src="{{ asset('/pagina/js/functions.js')}}"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
     <script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>

     <script>
     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
     
     let days_wekend = [0,1,2,3,4,5,6];
     let days_wekend_pakg = new Array();
     <?php  foreach ($packages as $key => $pk) :?>
          days_wekend_pakg[<?=$pk->id?>] = new Array();
     <?php endforeach; ?>
     <?php  foreach ($wkdy as $key => $days) :?>
               days_wekend_pakg[<?=$days->package_id?>].push(<?=$days->position?>);
          <?php endforeach; ?>
     console.log(days_wekend_pakg);
     
     let days_disabled = [];
     <?php  foreach ($disable_days as $key => $days) :?>
          days_disabled.push( '<?=$days->id?>');
     <?php endforeach; ?>

     var date = new Date();
     date.setDate(date.getDate()+1);
     
     // $('#piker').datepicker({
     //      // format: "dd-mm-yyyy",
     //      format: "yyyy-mm-dd",
     //      autoclose: true,
     //      todayHighlight: true,
     //      startDate: date,
     //      datesDisabled: days_disabled,
     //      daysOfWeekDisabled: "0,6",
     // });

     
     const diferenciaDeArreglos = (arr1, arr2) => {
	     return arr1.filter(elemento => arr2.indexOf(elemento) == -1);
     }

     

     function cambiar_precio(sel) {
          var costo  = $('#'+sel.id).find(':selected').data('cost');
          $('#pri_'+sel.id).text(costo);
     }

     function solicitar(id) {
          $("#piker").val("");
          $("#piker").datepicker("destroy");
          let bloc =  diferenciaDeArreglos(  days_wekend, days_wekend_pakg[id] ) ;

          console.log(bloc);

          $('#piker').datepicker({
               format: "yyyy-mm-dd",
               autoclose: true,
               todayHighlight: true,
               startDate: date,
               datesDisabled: days_disabled,
               daysOfWeekDisabled: bloc.toString(),
          });


          $('#package_id').val(id);
          var dat  = $('#selec_package_'+id).find(':selected');
          var horas = dat.val();
          var costo = dat.data('cost');
          $('#package_horas').val(horas);
          $('#package_costo').val(costo);
          $('#exampleModal').modal('show');
     }

     $(function() {
          //hang on event of form with id=myform
          $("#groupForm").submit(function(e) {

               //prevent Default functionality
               e.preventDefault();

               //get the action-url of the form
               // var formData = new FormData($("#groupForm")[0]);

               //do your own request an handle the results
               $.ajax({
                    url: "{{ route('register_appointment') }}",
                    type: 'POST',
                    dataType: 'json',
                    data: $('#groupForm').serialize(),
                    success: function(data) {
                         Swal.fire({
                              position: 'top-end',
                              icon: 'success',
                              title: 'Submitted successfully',
                              showConfirmButton: false,
                              timer: 1500
                         });
                         $('#exampleModal').modal('hide');
                         $('#groupForm')[0].reset();
                    },
                    error: function (xhr, status, errorThrown) {
                         Swal.fire({
                              position: 'top-end',
                              icon: 'error',
                              title: 'Error sending, try again later',
                              showConfirmButton: false,
                              timer: 1500
                         });
                    }
               });

          });

     });
     </script>

</body>
</html>
