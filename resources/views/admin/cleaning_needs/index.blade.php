@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Necesidades de limpieza</h3>
               <!-- <div class="card-tools">
                    <a href="{{ route('admin.packages.create') }}" class="btn btn-success btn-sm">
                         <i class="fa fa-plus"></i> Nuevo
                    </a>
               </div> -->
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Titulo</th>
                              <th>Icono</th>
                              <th class="text-center">Estatus</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($cleaningneeds as $cleaning_need)
                         <tr>
                              <td>{{ $cleaning_need->id }}</td>
                              <td>{{ $cleaning_need->name }}</td>
                              <td>{{ $cleaning_need->title }}</td>
                              <td>
                                   <img src="
                                   @if($cleaning_need->icon)
                                        {{ $cleaning_need->icon }}
                                   @else
                                        https://webdevtrick.com/wp-content/uploads/preview-img.jpg
                                   @endif
                                   " class="img-fluid" style="width:40px;" alt="your image" title=''/>
                              </td>
                              <td class="text-center">
                                   @if($cleaning_need->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td class="text-center">
                                   <!-- <a href="{{ route('admin.packages.show', $cleaning_need) }}"
                                        class="btn btn-sm  btn-outline-primary"
                                   > <i class="fas fa-address-card"></i> </a> -->

                                   <a href="{{ route('admin.cleaning_need.edit', $cleaning_need) }}"
                                        class="btn btn-sm btn-outline-info"
                                   > <i class="fas fa-edit"></i> </a>
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
