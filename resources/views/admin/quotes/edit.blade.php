@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" enctype="multipart/form-data" action="{{ route('admin.quotes.update', $quotes) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Nueva cita</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name', $quotes->name) }}" required placeholder="Nombre">
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Teléfono</label><br>
                                             <input type="text" name="phone" class="form-control" value="{{ old('phone', $quotes->phone) }}" required placeholder="Teléfono" >
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Correo</label>
                                             <input type="email" name="email" class="form-control" value="{{ old('mail', $quotes->mail) }}" required placeholder="Correo">
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Día de visita</label>
                                             <input type="text" name="visit" class="form-control" id="piker" value="{{ old('visit',  \Carbon\Carbon::parse($quotes->visit)->format('d-m-Y')) }}" required placeholder="Día de visita">
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Paquete</label>
                                             <select class="form-control" name="package_id" id="package_id" onchange="cambiar_precio()">
                                                  <?php foreach ($packages as $key => $package): ?>
                                                       <option
                                                       data-cost="{{ $package->price }}"
                                                       <?php if ($package->id == $quotes->package_id ): ?>
                                                            selected
                                                       <?php endif; ?>
                                                        value="{{$package->id}}">{{$package->name}}</option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Horas</label>
                                             <select class="form-control" name="hours" id="hours" onchange="cambiar_precio()">
                                                  <?php for ($i=1; $i < 6; $i++) {
                                                       $sel = '';
                                                       if ($i == $quotes->hours ){
                                                            $sel = 'selected';
                                                       }
                                                       echo "<option value='".$i."' ".$sel.">".$i." Horas</option>";
                                                  } ?>
                                             </select>
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Costo</label>
                                             <input type="text" name="cost" class="form-control" id="cost" value="{{ old('cost',  $quotes->cost) }}" required placeholder="Costo" readonly="readonly">
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Dirección</label>
                                             <textarea name="direction" rows="4" cols="80" class="form-control" required placeholder="Dirección">{{ old('direction', $quotes->direction) }}</textarea>
                                        </div>
                                   </div>

                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Editar cita</button>
                    <a href="{{ route('admin.calendar.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
var date = new Date();
date.setDate(date.getDate()+1);
$('#piker').datepicker({
     format: "dd-mm-yyyy",
     autoclose: true,
     todayHighlight: true,
     startDate: date
});
$(function () {
     $("input[data-bootstrap-switch]").each(function(){
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
     });
});
function cambiar_precio() {
     var horas = $('#package_id').find(':selected').data('cost');
     var costo = $('#hours').val();
     $('#cost').val(horas * costo);
}
</script>

@endpush
