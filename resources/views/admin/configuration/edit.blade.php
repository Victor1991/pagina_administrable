@extends('admin.layout')

@section('content')
<div class="col-md-12 ml-auto mr-auto">
     <form class="form" method="post" action="{{ route('admin.configuration.update', $configuration) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Editar información del sitio</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-facebook-square"></i> Facebook</label>
                                             <input type="url" name="facebook" class="form-control" value="{{ old('facebook', $configuration->facebook) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-linkedin"></i> LinkedIn</label>
                                             <input type="url" name="linkedin" class="form-control" value="{{ old('linkedin', $configuration->linkedin) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-linkedin"></i> Instagram</label>
                                             <input type="url" name="instagram" class="form-control" value="{{ old('instagram', $configuration->instagram) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-twitter-square"></i> Twitter</label>
                                             <input type="url" name="twitter" class="form-control" value="{{ old('twitter', $configuration->twitter) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-youtube-square"></i> YouTube</label>
                                             <input type="url" name="youtube" class="form-control" value="{{ old('youtube', $configuration->youtube) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fab fa-snapchat-square"></i> Snapchat</label>
                                             <input type="url" name="snapchat" class="form-control" value="{{ old('snapchat', $configuration->snapchat) }}" placeholder="URL Canal">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fas fa-envelope-square"></i> Correo</label>
                                             <input type="email" name="mail" class="form-control" value="{{ old('mail', $configuration->mail) }}" placeholder="Correo">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fas fa-address-card"></i> Dirección</label>
                                             <input type="text" name="address" class="form-control" value="{{ old('address', $configuration->address) }}" placeholder="Dirección">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name"><i class="fas fa-phone-square"></i> Teléfono</label>
                                             <input type="text" name="phone" class="form-control" value="{{ old('phone', $configuration->phone) }}" placeholder="Teléfono">
                                        </div>
                                   </div>
                              </div>
                         </div>

                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Editar información</button>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')

@endpush
