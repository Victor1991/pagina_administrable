@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" action="{{ route('admin.user.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Registrar Administrador</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-8">
                                        <div class="form-group">
                                             <label for="name">Nombre(s)</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre">
                                        </div>
                                   </div>
                                   <div class="col-md-4 text-center">
                                        <label for="email">Acceso</label><br>
                                        <input type="checkbox" name="status" value="1" checked data-bootstrap-switch >
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Apellido Paterno</label>
                                             <input type="text" name="last_name_paternal" class="form-control" value="{{ old('last_name_paternal') }}" placeholder="Apellido Paterno">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Apellido Materno</label>
                                             <input type="text" name="last_name_maternal" class="form-control" value="{{ old('last_name_maternal') }}" placeholder="Apellido Materno">
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Correo</label>
                                             <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Correo">
                                        </div>
                                   </div>

                              </div>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                         <div class="col-md-12">
                              <span class="help-block"> La contraseña será generada y enviada al nuevo usuario vía correo.</span>
                         </div>
                         <div class="col-md-12">
                              <hr>
                         </div>
                    </div>


               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Agregar administrador</button>
                    <a href="{{ route('admin.user.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
$(function () {
     $("input[data-bootstrap-switch]").each(function(){
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
     });
});
</script>

@endpush
