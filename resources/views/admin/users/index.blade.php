@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Administradores</h3>
               <div class="card-tools">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-success btn-sm">
                         <i class="fa fa-plus"></i> Nuevo
                    </a>
               </div>
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th>Correo</th>
                              <th class="text-center">Estatus</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($users as $user)
                         <tr>
                              <td>{{ $user->id }}</td>
                              <td>{{ $user->name }} {{ $user->last_name_paternal }} {{ $user->last_name_maternal }}</td>
                              <td>{{ $user->email }}</td>
                              <td class="text-center">
                                   @if($user->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>
                              <td class="text-center">
                                   <!-- <a href="{{ route('admin.user.show', $user) }}"
                                        class="btn btn-sm  btn-outline-primary"
                                   > <i class="fas fa-address-card"></i> </a> -->

                                   <a href="{{ route('admin.user.edit', $user) }}"
                                        class="btn btn-sm btn-outline-info"
                                   > <i class="fas fa-user-edit"></i> </a>

                                   <form method="POST"
                                        action="{{ route('admin.user.destroy', $user) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-outline-danger"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar este administrador ?')"
                                         >
                                        <i class="fas fa-user-times"></i>
                                        </button>
                                   </form>
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
