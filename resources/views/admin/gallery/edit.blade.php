@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" enctype="multipart/form-data" action="{{ route('admin.gallery.update', $images) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Editar imagen</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Imagen</label>
                                             <div class="row">
                                                  <div class="col-md-12 div_img">
                                                       <div class="alert"></div>
                                                       <div id='img_container'><img id="preview" src="
                                                            @if($images->photo)
                                                                 {{ $images->photo }}
                                                            @else
                                                                 https://webdevtrick.com/wp-content/uploads/preview-img.jpg
                                                            @endif
                                                            " alt="your image" title=''/></div>
                                                       <div class="input-group">
                                                            <div class="custom-file">
                                                                 <input type="file" name="photo" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" accept="image/*">
                                                                 <label class="custom-file-label" for="inputGroupFile01">Elija el archivo</label>
                                                            </div>
                                                       </div>
                                                  </div>

                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>

                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Editar imagen</button>
                    <a href="{{ route('admin.gallery.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<style media="screen">
.alert{
     text-align:center;
}

#preview{
     max-height:256px;
     height:auto;
     width:auto;
     display:block;
     margin-left: auto;
     margin-right: auto;
     padding:5px;
}
#img_container{
     border-radius:5px;
     margin-top:20px;
     width:auto;
}

.imgInp{
     width:150px;
     margin-top:10px;
     padding:10px;
     background-color:#d3d3d3;
}
.loading{
     animation:blinkingText ease 2.5s infinite;
}
@keyframes blinkingText{
     0%{     color: #000;    }
     50%{   color: #transparent; }
     99%{    color:transparent;  }
     100%{ color:#000; }
}
.custom-file-label{
     cursor:pointer;
}

.div_img{
     background-color: #eee;
     border-radius: 5px;
     padding: 10px;
}
.custom-file-input:lang(en) ~ .custom-file-label::after {
    content: "Navegar";
}
</style>
@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
// CKEDITOR.replace( 'description' );
$(function () {
     $("input[data-bootstrap-switch]").bootstrapSwitch();
});
$("#inputGroupFile01").change(function(event) {
     RecurFadeIn();
     readURL(this);
});
$("#inputGroupFile01").on('click',function(event){
     RecurFadeIn();
});

$("#agregar_tarea").on('click',function(event){
     $( "#tareas" ).append( '<div style="display: flex; margin-bottom: 10px;"><input name="n[]"  class="form-control"  placeholder="tarea" ><button type="button" class="btn btn-sm btn-danger delete_task" ><i class="fa fa-trash" aria-hidden="true"></i></button></div>' );
});

$(document).on('click',".delete_task", function(){
      $(this).parent().remove();
})

function readURL(input) {
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $("#inputGroupFile01").val();
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               debugger;
               $('#preview').attr('src', e.target.result);
               $('#preview').hide();
               $('#preview').fadeIn(500);
               $('.custom-file-label').text(filename);
          }
          reader.readAsDataURL(input.files[0]);
     }
     $(".alert").removeClass("loading").hide();
}
function RecurFadeIn(){
     console.log('ran');
     FadeInAlert("Wait for it...");
}
function FadeInAlert(text){
     $(".alert").show();
     $(".alert").text(text).addClass("loading");
}
</script>

@endpush
