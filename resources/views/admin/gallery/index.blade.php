@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Galería</h3>
               <!-- <div class="card-tools">
                    <a href="{{ route('admin.gallery.create') }}" class="btn btn-success btn-sm">
                         <i class="fa fa-plus"></i> Nuevo
                    </a>
               </div> -->
          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th class="text-center">Imagen</th>
                              <!-- <th class="text-center">Estatus</th> -->
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($images as $img)
                         <tr>
                              <td>{{ $img->id }}</td>
                              <td class="text-center"> <img src="{{ $img->photo }}" class="img-fluid" style="width:80px;" alt=""> </td>
                              <!-- <td class="text-center">
                                   @if($img->status == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td> -->
                              <td class="text-center">
                                   <!-- <a href="{{ route('admin.packages.show', $img) }}"
                                        class="btn btn-sm  btn-outline-primary"
                                   > <i class="fas fa-address-card"></i> </a> -->

                                   <a href="{{ route('admin.gallery.edit', $img) }}"
                                        class="btn btn-sm btn-outline-info"
                                   > <i class="fas fa-edit"></i> </a>

                                   <!-- <form method="POST"
                                        action="{{ route('admin.gallery.destroy', $img) }}"
                                        style="display:inline">
                                        {{ csrf_field() }} {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-outline-danger"
                                             onclick="return confirm('¿ Estás seguro de querer eliminar esta imagen ?')"
                                         >
                                        <i class="fas fa-trash"></i>
                                        </button>
                                   </form> -->
                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
