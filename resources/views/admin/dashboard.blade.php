@extends('admin.layout')

@section('content')
<!-- Info boxes -->
<div class="row">
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
               <span class="info-box-icon bg-info elevation-1"><i class="fa fa-eye" aria-hidden="true"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Vistas</span>
                    <span class="info-box-number">
                         {{ $page_views->count() }}
                    </span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
     <!-- /.col -->
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
               <span class="info-box-icon bg-danger elevation-1"><i class="far fa-calendar-alt"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Citas</span>
                    <span class="info-box-number">0</span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
     <!-- /.col -->

     <!-- fix for small devices only -->
     <div class="clearfix hidden-md-up"></div>

     <!-- /.col -->
     <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
               <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

               <div class="info-box-content">
                    <span class="info-box-text">Usuarios</span>
                    <span class="info-box-number">{{ $user->count() }}</span>
               </div>
               <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
     </div>
     <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h5 class="card-title">Informe de citas del mes</h5>

                    <div class="card-tools">
                         <button type="button" class="btn btn-tool" data-card-widget="collapse">
                              <i class="fas fa-minus"></i>
                         </button>
                         <button type="button" class="btn btn-tool" data-card-widget="remove">
                              <i class="fas fa-times"></i>
                         </button>
                    </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                    <div class="row">
                         <div class="col-md-12">

                              <div id="calendar"></div>

                         </div>

                    </div>
                    <!-- /.row -->
               </div>

          </div>
     </div>
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h5 class="card-title">Informe de citas del mes</h5>

                    <div class="card-tools">
                         <button type="button" class="btn btn-tool" data-card-widget="collapse">
                              <i class="fas fa-minus"></i>
                         </button>
                         <button type="button" class="btn btn-tool" data-card-widget="remove">
                              <i class="fas fa-times"></i>
                         </button>
                    </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                    <div class="row">
                         <div class="col-md-12">

                              <div id="container" ></div>

                              <!-- /.chart-responsive -->
                         </div>

                    </div>
                    <!-- /.row -->
               </div>

          </div>
          <!-- /.card -->
     </div>
     <!-- /.col -->
</div>
<!-- /.row -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <div class="modal-content">
               <div class="modal-header" style="background-color: #007bff; color:#fff;">
                    <h5 class="modal-title" id="exampleModalLabel">Ver evento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="hidden" id="load_modal" style="width: 100%; background-color: #000000c4; height: 100%; position: absolute; z-index: 999; text-align:center;">
                    <i class="fas fa-circle-notch fa-spin fa-5x" style="color:#fff; margin-top: 25%;"></i>
               </div>
               <div class="modal-body" id="content_modal">

                    <div class="col-md-12">
                         <p style="font-size:15px;">
                              <b> <i class="fas fa-box"></i> Paquete : </b><span id="mod_paquete"></span><br>
                              <b> <i class="fa fa-user"></i> Nombre : </b><span id="mod_name"></span><br>
                              <b> <i class="fa fa-calendar"></i> Fecha de visita : </b> <span id="mod_visit"></span><br>
                              <b> <i class="fa fa-phone"></i> Teléfono : </b>  <span id="mod_phone"></span><br>
                              <b> <i class="fa fa-envelope"></i> Correo : </b>  <span id="mod_mail"></span><br>
                              <b> <i class="fas fa-clock"></i> Horas : </b>  <span id="mod_hours"></span><br>
                              <b> <i class="fas fa-dollar-sign"></i> Costo : </b>  <span id="mod_cost"></span><br>
                              <b> <i class="fas fa-map-signs"></i> Dirección : </b>  <span id="mod_direction"></span><br>
                         </p>
                    </div>
               </div>
               <div class="modal-footer">
                    <div class="col-sm-12">

                    <div class="row">
                         <div class="col-sm-6">
                              <a type="button" href="" id="btn_finalizar" class="btn btn-success" > Marcar como terminado </a>
                         </div>
                         <div class="col-sm-6 text-right">
                              <a type="button" id="btn_editar" class="btn btn-warning" > Editar </a>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                         </div>

                    </div>
               </div>
               </div>
          </div>
     </div>
</div>

<!-- Modal -->
<div class="modal fade" id="disbale_day" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
     <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
               <form action="{{ route('admin.disable_day.store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body text-center">
                         <div class="row">
                              <div class="col-md-12">
                                   <h5>
                                        <label for=""> Deshabilitar día </label><br>
                                        <label id="fecha_des">03-03-2020</label>
                                   </h5> 
                              </div>
                              <div class="col-md-12">
                                   <input type="hidden" name="fec_id" id="fec_id">
                                   <input type="hidden" name="fec_dis" id="fec_dis">
                                   <input type="checkbox" name="status" id="n_status" value="1"  data-bootstrap-switch >
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer" style="padding: 2px;">
                         <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                         <button type="submit" class="btn btn-warning btn-sm">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>

<script>
     $('#exampleModal').on('show.bs.modal', event => {
          var button = $(event.relatedTarget);
          var modal = $(this);          
     });
</script>


@stop
@push('scripts')
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/data.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="https://code.highcharts.com/stock/modules/export-data.js"></script>
<!-- Page specific script -->


<script src="{{ asset('/theme/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/es-us.min.js" integrity="sha512-rW9yo+wCSOiuvTcjeYtoihJoZHWXYWqPNdNs1hpMLn+Gz8THZaHpU10qbxdNlcydbUZghThvVR5KevjTHWM2ww==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>


<script>

$(function () {
     $("#n_status").bootstrapSwitch();
});

// Highcharts.getJSON('https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/new-intraday.json', function (data) {
Highcharts.getJSON("{{ route('data_view_json') }}", function (data) {
     Highcharts.setOptions({
          lang: {
               loading: 'Cargando...',
               months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
               weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
               shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
               exportButtonTitle: "Exportar",
               printButtonTitle: "Importar",
               rangeSelectorFrom: "Desde",
               rangeSelectorTo: "Hasta",
               rangeSelectorZoom: "Período",
               downloadPNG: 'Descargar imagen PNG',
               downloadJPEG: 'Descargar imagen JPEG',
               downloadPDF: 'Descargar imagen PDF',
               downloadSVG: 'Descargar imagen SVG',
               printChart: 'Imprimir',
               resetZoom: 'Reiniciar zoom',
               resetZoomTitle: 'Reiniciar zoom',
               thousandsSep: ",",
               decimalPoint: '.'
          }
     });

     // create the chart
     Highcharts.stockChart('container', {

          title: {
               text: 'Visitas diarias en el sitio'
          },

          subtitle: {
               text: ''
          },

          xAxis: {
               gapGridLineWidth: 0
          },

          rangeSelector: {
               buttons: [{
                    type: 'hour',
                    count: 1,
                    text: '1h'
               }, {
                    type: 'day',
                    count: 1,
                    text: '1D'
               }, {
                    type: 'all',
                    count: 1,
                    text: 'All'
               }],
               selected: 1,
               inputEnabled: false
          },

          series: [{
               name: 'Vistas',
               type: 'area',
               data: data,
               gapSize: 5,
               fillColor: {
                    linearGradient: {
                         x1: 0,
                         y1: 0,
                         x2: 0,
                         y2: 1
                    },
                    stops: [
                         [0, Highcharts.getOptions().colors[0]],
                         [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
               },
               threshold: null
          }]
     });
});

/* initialize the calendar
-----------------------------------------------------------------*/
//Date for the calendar events (dummy data)
var date = new Date()
var d    = date.getDate(),
m    = date.getMonth(),
y    = date.getFullYear()



var Calendar = FullCalendar.Calendar;
var Draggable = FullCalendarInteraction.Draggable;

var containerEl = document.getElementById('external-events');
var checkbox = document.getElementById('drop-remove');
var calendarEl = document.getElementById('calendar');

// initialize the external events
// -----------------------------------------------------------------
let days_disabled = [];
<?php 
     foreach ($disable_days as $key => $days) :?>
         days_disabled.push( '<?=$days->day?>');
     <?php endforeach;
?>

var calendar = new Calendar(calendarEl, {
     plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
     locale: 'es',
     'themeSystem': 'bootstrap',
     //Random default events
     events    : [
          <?php foreach ($quotes as $key => $qts): ?>
          {
               title          : 'visita a \n  {{ $qts->name }} ' ,
               start          : new Date('{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $qts->visit)->format("Y-m-d") }}T13:07:20'),
               <?php if ($qts->finalized == 1):?>
                    backgroundColor: '#1f9b1b',
               <?php else: ?>
                    backgroundColor: '#00cce1',
               <?php endif; ?>
               borderColor    : '#000',
               className      : "cumpleanios_event",
               id       : "{{ route('admin.quotes.show', $qts) }}",
          },
          <?php endforeach; ?>
     ],
     dateClick: function(info) {
         get_day_disable(info.dateStr);
     }, 
     dayRender: function (date_dy, cell) {
          let f_c = date_dy.date.getFullYear() +'-' + (date_dy.date.getMonth()+1).toString().padStart(2, "0") +'-'+ (date_dy.date.getDate().toString().padStart(2, "0"));
          var n = days_disabled.includes(f_c);
          if (n) {
               console.log(date_dy.el.style.backgroundColor = "#dedede");
          }        
     },
     eventClick: function(info) {
          $( "#load_modal" ).removeClass( "hidden" );
          var eventObj = info.event;
          $('#exampleModal').modal('show');
          get_evento(eventObj.id);
      },
     editable  : true,
     droppable : true, // this allows things to be dropped onto the calendar !!!

});

calendar.render();


function get_evento(url_link) {

     $.ajax({
          type: "GET",
          dataType: 'json',
          url: url_link,
          async: false,
          beforeSend: null,
          success: function(val)
          {

               var date_visit = new Date(val.visit);
               $('#mod_name').text(val.name);
               $('#mod_visit').text(date_visit.getDate()+'-'+date_visit.getMonth()+1+'-'+date_visit.getFullYear());
               $('#mod_phone').text(val.phone);
               $('#mod_mail').text(val.mail);
               $('#mod_hours').text(val.hours);
               $('#mod_cost').text(val.cost);
               $('#mod_direction').text(val.direction);
               $('#mod_paquete').text(val.package.name);
               if (val.finalized == 1) {
                    $('#btn_finalizar').hide();
                    $('#btn_editar').hide();
               }else {
                    var url_edit = 'admin/quotes/'+val.id+'/edit';
                    var url_finalizar = 'admin/quotes/finish_quote/'+val.id;
                    $('#btn_finalizar').attr('href', url_finalizar);
                    $('#btn_editar').attr('href', url_edit);
               }
               $( "#load_modal" ).addClass( "hidden" );

          }
     });
}

function get_day_disable(day) {
     let ndate =  moment(day,'YYYY-MM-DD').format('DD-MM-YYYY');
     $('#fecha_des').html(ndate);
     $("#fec_dis").val(day);
     $("#fec_id").val('');
     $.ajax({
          type: "GET",
          dataType: 'json',
          url: "{{ url('admin/disable_day')}}/"+ day,
          async: false,
          beforeSend: null,
          success: function(val)
          {
               if(val.id){
                    $("#fec_id").val(val.id);
                    if(val.status){
                         $("#n_status").bootstrapSwitch('state', true);
                    }else{
                         $("#n_status").bootstrapSwitch('state', false);
                    }
               }else{
                    $("#n_status").bootstrapSwitch('state', false);
               }
               $('#disbale_day').modal('show');
          }

     });
}

$("#btn_finalizar").click(function(event) {
     swal({
          title: "Finalizar evento",
          text: '¿ desea finalizar el vento ?',
          icon: "info",
          buttons: true,
          dangerMode: true,
     })
     .then((willDelete) => {
          if (willDelete) {
               $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: $(this).attr('href'),
                    async: false,
                    beforeSend: null,
                    success: function(val)
                    {
                         location.reload();
                    }
               });

          }
     });
     event.preventDefault();
});

</script>

@endpush

@push('style')

<!-- fullCalendar -->
<!-- fullCalendar -->
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">

<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
</style>
<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
.hidden{
     display: none;
}
</style>

@endpush
