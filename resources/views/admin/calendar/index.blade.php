@extends('admin.layout')

@section('content')

<div class="row">
     <!-- /.col -->
     <div class="col-md-12">
          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Calendario de eventos</h3>
                    <div class="card-tools">
                         <a href="{{ route('admin.quotes.create') }}" class="btn btn-success btn-sm">
                              <i class="fa fa-plus"></i> Nuevo
                         </a>
                    </div>
               </div>
               <div class="card-body ">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
               </div>
               <!-- /.card-body -->
          </div>
          <!-- /.card -->
     </div>
     <!-- /.col -->
</div>
@stop
@push('scripts')
<script src="{{ asset('/theme/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/es-us.min.js" integrity="sha512-rW9yo+wCSOiuvTcjeYtoihJoZHWXYWqPNdNs1hpMLn+Gz8THZaHpU10qbxdNlcydbUZghThvVR5KevjTHWM2ww==" crossorigin="anonymous"></script>
<!-- Page specific script -->
<script>
$(function () {

     /* initialize the calendar
     -----------------------------------------------------------------*/
     //Date for the calendar events (dummy data)
     var date = new Date()
     var d    = date.getDate(),
     m    = date.getMonth(),
     y    = date.getFullYear()

     var Calendar = FullCalendar.Calendar;
     var Draggable = FullCalendarInteraction.Draggable;

     var containerEl = document.getElementById('external-events');
     var checkbox = document.getElementById('drop-remove');
     var calendarEl = document.getElementById('calendar');

     // initialize the external events
     // -----------------------------------------------------------------


     var calendar = new Calendar(calendarEl, {
          plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
          buttonText: {
              prev: 'Anterior',
              next: 'Siguiente',
              today: 'Mes actual',
          },
          // header    : {
          //      left  : 'prev,next today',
          //      center: 'title',
          //      right : 'dayGridMonth,timeGridWeek,timeGridDay'
          // },
          locale: 'es',
          'themeSystem': 'bootstrap',
          //Random default events
          events    : [
               <?php foreach ($quotes as $key => $qts): ?>
               {
                    title          : 'visita a \n  {{ $qts->name }} ' ,
                    start          : new Date('{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $qts->visit)->format("Y-m-d") }}T13:07:20'),
                    <?php if ($qts->finalized == 1):?>
                         backgroundColor: '#1f9b1b',
                    <?php else: ?>
                         backgroundColor: '#00cce1',
                    <?php endif; ?>
                    borderColor    : '#000', //red
                    className      : "cumpleanios_event",
                    url            : '{{ url("admin/quotes/$qts->id/edit") }}',
               },
               <?php endforeach; ?>
          ],
          editable  : true,
          droppable : true, // this allows things to be dropped onto the calendar !!!

     });

     calendar.render();
     // $('#calendar').fullCalendar()

})
</script>

@endpush

@push('style')
<!-- fullCalendar -->
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-daygrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-timegrid/main.min.css')}}">
<link rel="stylesheet" href="{{ asset('/theme/plugins/fullcalendar-bootstrap/main.min.css')}}">
<style>
.info_card{
     text-align:center;
     background-color: rgba(10, 10, 10, 0.66);
     margin-top:80px;
     margin-bottom:80px;
     padding: 80px;
     border-radius: 15px;
}
.text-sm .card-title {
    font-size: 2rem;
}
</style>
@endpush
