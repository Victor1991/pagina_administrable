@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" action="{{ route('admin.quotes.store') }}">
          {{ csrf_field() }}

          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Nueva cita</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Nombre</label>
                                             <input type="text" name="name" class="form-control" value="{{ old('name') }}" required placeholder="Nombre">
                                        </div>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Teléfono</label><br>
                                             <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" required placeholder="Teléfono" >
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="name">Correo</label>
                                             <input type="email" name="email" class="form-control" value="{{ old('email') }}" required placeholder="Correo">
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Día de visita</label>
                                             <input type="text" name="visit" class="form-control" id="piker" value="{{ old('visit') }}" required placeholder="Día de visita">
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="last_name_maternal">Paquete</label>
                                             <select class="form-control" name="package_id">
                                                  <?php foreach ($packages as $key => $package): ?>
                                                       <option value="{{$package->id}}">{{$package->name}}</option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>

                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Dirección</label>
                                             <textarea name="direction" rows="4" cols="80" class="form-control" required placeholder="Dirección">{{ old('direction') }}</textarea>
                                        </div>
                                   </div>

                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Agregar cita</button>
                    <a href="{{ route('admin.calendar.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
var date = new Date();
date.setDate(date.getDate()+1);
$('#piker').datepicker({
     format: "dd-mm-yyyy",
     autoclose: true,
     todayHighlight: true,
     startDate: date
});
$(function () {
     $("input[data-bootstrap-switch]").each(function(){
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
     });
});
</script>

@endpush
