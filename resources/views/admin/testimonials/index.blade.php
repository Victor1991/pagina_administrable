@extends('admin.layout')

@section('content')
<div class="col-md-12">
     <div class="card ">
          <div class="card-header ">
               <h3 class="card-title">Paquetes</h3>

          </div>
          <div class="card-body table-responsive">
               <table class="table table-hover table-striped table_js">
                    <thead>
                         <tr>
                              <th>ID</th>
                              <th>Nombre</th>
                              <th class="text-center">Calificación</th>
                              <th class="text-center">Vista en página</th>
                              <th class="text-center"><i class="nc-icon nc-preferences-circle-rotate"></i></th>
                         </tr>
                    </thead>
                    <tbody>
                         @foreach ($testimonials as $testimonial)
                         <tr>
                              <td>{{ $testimonial->id }}</td>
                              <td>{{ $testimonial->quote->name }}</td>
                              <td class="text-center">{{ $testimonial->qualification }}</td>
                              <td class="text-center">
                                   @if($testimonial->on_page == 1)
                                        <h6><span class="badge badge-success">Activo</span></h6>
                                   @else
                                        <h6><span class="badge badge-danger">Inactivo</span></h6>
                                   @endif
                              </td>

                              <td class="text-center">
                                   <a href="{{ route('admin.testimonials.edit', $testimonial) }}"
                                        class="btn btn-sm btn-outline-info"
                                   > <i class="fas fa-edit"></i> </a>

                              </td>
                         </tr>
                         @endforeach
                    </tbody>
               </table>
          </div>
     </div>
</div>
@stop
