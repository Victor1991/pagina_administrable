@extends('admin.layout')

@section('content')
<div class="col-md-8 ml-auto mr-auto">
     <form class="form" method="post" enctype="multipart/form-data" action="{{ route('admin.testimonials.update', $testimonial) }}">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="card">
               <div class="card-header ">
                    <h3 class="card-title">Nueva cita</h3>
               </div>
               <div class="card-body ">
                    <div class="row">
                         <div class="col-md-12">
                              @if($errors->any())
                              <ul class="list-group">
                                   @foreach($errors->all() as $key => $error)
                                   <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                   @endforeach
                              </ul>
                              @endif
                         </div>
                         <div class="col-md-12">
                              <div class="row">

                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name">Calificación</label><br>
                                             <p class="clasificacion" style="margin-bottom: 5px;">
                                                  <input id="radio1" type="radio" name="qualification" value="5" <?php if ( $testimonial->qualification == 5): ?> checked <?php endif; ?>>
                                                  <label for="radio1">★</label>
                                                  <input id="radio2" type="radio" name="qualification" value="4" <?php if ( $testimonial->qualification == 4): ?> checked <?php endif; ?>>
                                                  <label for="radio2">★</label>
                                                  <input id="radio3" type="radio" name="qualification" value="3" <?php if ( $testimonial->qualification == 3): ?> checked <?php endif; ?>>
                                                  <label for="radio3">★</label>
                                                  <input id="radio4" type="radio" name="qualification" value="2" <?php if ( $testimonial->qualification == 2): ?> checked <?php endif; ?>>
                                                  <label for="radio4">★</label>
                                                  <input id="radio5" type="radio" name="qualification" value="1" <?php if ( $testimonial->qualification == 1): ?> checked <?php endif; ?>>
                                                  <label for="radio5">★</label>
                                             </p>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="name">Ver en la página</label><br>
                                             <input type="checkbox" name="on_page" value="1" <?php if(old('on_page', $testimonial->on_page) == 1): ?> checked <?php endif; ?> data-bootstrap-switch >
                                        </div>
                                   </div>



                                   <div class="col-md-12">
                                        <div class="form-group">
                                             <label for="email">Descripción</label>
                                             <textarea name="description" rows="4" cols="80" class="form-control" required placeholder="Dirección">{{ old('description', $testimonial->description) }}</textarea>
                                        </div>
                                   </div>

                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <button type="submit" class="btn btn-info btn-fill float-right">Editar cita</button>
                    <a href="{{ route('admin.calendar.index') }}" class="btn btn-default btn-fill float-left">Cancelar</a>
                    <div class="clearfix"></div>
               </div>
          </div>

     </form>
</div>
@stop

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<style>
p.clasificacion {
     position: relative;
     overflow: hidden;
     display: inline-block;
}

p.clasificacion input {
     position: absolute;
     top: -100px;
}

p.clasificacion label {
     float: right;
     color: #333;
     font-size: 30px;
}

p.clasificacion label:hover,
p.clasificacion label:hover ~ label,
p.clasificacion input:checked ~ label {
     color: #dd4;
}
</style>
@endpush


<!-- Escripts pasarlos al layout -->
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('/theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>

<script>
$(function () {
     $("input[data-bootstrap-switch]").bootstrapSwitch();
});

var date = new Date();
date.setDate(date.getDate()+1);
$('#piker').datepicker({
     format: "dd-mm-yyyy",
     autoclose: true,
     todayHighlight: true,
     startDate: date
});
$(function () {
     $("input[data-bootstrap-switch]").each(function(){
          $(this).bootstrapSwitch('state', $(this).prop('checked'));
     });
});
</script>

@endpush
