<!-- Sidebar Menu -->
<nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->

          <li class="nav-item">
               <a href="{{ route('dashboard') }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                         Dashboard
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.user.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                         Administradores
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.calendar.index') }}" class="nav-link">
                    <i class="nav-icon far fa-calendar-alt"></i>
                    <p>
                         Calendario
                    </p>
               </a>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.packages.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-box"></i>
                    <p>
                         Paquetes
                    </p>
               </a>
          </li>
          <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                         Página
                         <i class="fas fa-angle-left right"></i>
                    </p>
               </a>
               <ul class="nav nav-treeview" >
                    <li class="nav-item">
                         <a href="{{ route('admin.cleaning_need.index') }}" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Necesidades de limpieza</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{ route('admin.gallery.index') }}" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Galería</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{ route('admin.testimonials.index') }}" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Testimonios</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{ route('admin.contact_images.index') }}" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Galería contacto</p>
                         </a>
                    </li>
                    <!-- <li class="nav-item">
                         <a href="#" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Galería Contacto</p>
                         </a>
                    </li>
                    <li class="nav-item">
                         <a href="#" class="nav-link">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Testimonios</p>
                         </a>
                    </li> -->
               </ul>
          </li>
          <li class="nav-item">
               <a href="{{ route('admin.configuration.edit', 1) }}" class="nav-link">
                    <i class="nav-icon fas fa-cog"></i>
                    <p>
                         Configuraciones
                    </p>
               </a>
          </li>

     </ul>
</nav>
<!-- /.sidebar-menu -->
